<?php 
include_once('helpers/db.php');

$group = [];

connect('localhost', 'root', '', 'groupdb', function() {
	if ($_POST) {
		$group_number = $_POST['group-number'];
		$group_lead = $_POST['group-lead']; 
		$faculty = $_POST['faculty'];

		$sql = "UPDATE `group` SET faculty = '$faculty' , lead = '$group_lead', number = '$group_number' WHERE id = 1";
		request($sql, false, function() { 
			header("Location: index.php");
		});
	}

	$sql = "SELECT * FROM `group`";
	request($sql, true, function($response) {
		$GLOBALS['group'] = $response[0];
	});
});
?>
<html>
<head>
	<title>Edit group info</title>
	<link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
	<div class="container">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h1 class="panel-title">Edit group info</h1>
			</div>
			<div class="panel-body">
				<form action="<?php $_PHP_SELF ?>" method="POST" role="form">
					<legend>Edit group info</legend>
					<div class="form-group">
						<label for="group-number">Group number</label>
						<input type="number" class="form-control" id="group-number" name="group-number" value=<?php echo $group['number'] ?>>
					</div>
					<div class="form-group">
						<label for="group-lead">Leader</label>
						<input type="text" class="form-control" id="group-lead" name="group-lead" size=15  value=<?php echo $group['lead'] ?>>
					</div>
					<div class="form-group">
						<label for="faculty">Faculty</label>
						<input type="text" class="form-control" id="faculty" name="faculty" size=15 value=<?php echo $group['faculty'] ?>>
					</div>
					<button type="submit" class="btn btn-primary">Save</button>
				</form>
			</div>
		</div>
	</div>
</body>
</html>