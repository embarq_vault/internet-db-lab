-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 25 2016 г., 09:36
-- Версия сервера: 5.5.48
-- Версия PHP: 5.4.45

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `groupdb`
--

-- --------------------------------------------------------

--
-- Структура таблицы `group`
--

CREATE TABLE IF NOT EXISTS `group` (
  `id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `faculty` varchar(15) NOT NULL,
  `lead` varchar(15) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `group`
--

INSERT INTO `group` (`id`, `number`, `faculty`, `lead`) VALUES
(1, 205, 'Computer-Scienc', 'Me');

-- --------------------------------------------------------

--
-- Структура таблицы `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `id` int(11) NOT NULL,
  `name` varchar(15) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `birthdate` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `students`
--

INSERT INTO `students` (`id`, `name`, `gender`, `birthdate`) VALUES
(5, 'Todd', 'Male', '2015-11-23'),
(6, 'Frank Kim', 'Male', '2016-03-20'),
(7, 'Phillip Ryan', 'Male', '2016-01-03'),
(8, 'Margaret Holmes', 'Female', '2015-08-15'),
(9, 'Robin Carpenter', 'Female', '2016-05-12'),
(10, 'Jean Wallace', 'Female', '2016-04-18'),
(11, 'Cynthia Ryan', 'Female', '2015-09-28'),
(12, 'Kathryn Kennedy', 'Female', '2015-07-15'),
(13, 'Roy Wheeler', 'Male', '2016-01-28'),
(14, 'Alice Arnold', 'Female', '2016-02-04'),
(15, 'Jonathan Riley', 'Male', '2015-06-01'),
(16, 'Ryan Sims', 'Male', '2015-06-28'),
(17, 'Clarence Fowler', 'Male', '2015-08-15'),
(18, 'Virginia Washin', 'Female', '2015-08-18'),
(19, 'Shirley Garza', 'Female', '2015-08-15'),
(20, 'Steven George', 'Male', '2015-09-03'),
(21, 'Kelly Adams', 'Female', '2015-07-31'),
(22, 'Juan Smith', 'Male', '2015-10-19'),
(23, 'Cynthia Fox', 'Female', '2015-12-03'),
(24, 'Harold Roberts', 'Male', '2015-12-10'),
(25, 'Kenneth Burton', 'Male', '2015-10-16'),
(26, 'Larry Knight', 'Male', '2015-07-29'),
(27, 'Victor Harris', 'Male', '2015-08-22'),
(28, 'Nicole Roberts', 'Female', '2016-03-28'),
(29, 'Joe Rodriguez', 'Male', '2016-02-29'),
(30, 'Gregory Reyes', 'Male', '2015-06-17'),
(31, 'Johnny Wells', 'Male', '2015-07-26'),
(32, 'Ashley Ferguson', 'Female', '2015-08-08'),
(33, 'Johnny Perkins', 'Male', '2016-01-14'),
(34, 'Douglas Collins', 'Male', '2015-06-20'),
(35, 'Lillian Ryan', 'Female', '2015-06-14'),
(36, 'Alan Morales', 'Male', '2015-07-16'),
(37, 'Randy Mcdonald', 'Male', '2015-09-19'),
(38, 'Eugene Bryant', 'Male', '2016-04-11'),
(39, 'Steven Martin', 'Male', '2015-05-23'),
(40, 'Lori Jenkins', 'Female', '2015-09-29'),
(41, 'Benjamin Bradle', 'Male', '2015-09-21'),
(42, 'Eugene White', 'Male', '2016-02-16'),
(43, 'Jacqueline Ryan', 'Female', '2015-10-21'),
(44, 'Lois Moreno', 'Female', '2015-11-21'),
(45, 'Robin Martin', 'Female', '2015-07-25'),
(46, 'Betty Alvarez', 'Female', '2015-08-21'),
(47, 'Judith Torres', 'Female', '2016-02-20'),
(48, 'Emily Hudson', 'Female', '2016-01-28'),
(49, 'Clarence Harris', 'Male', '2015-10-27'),
(50, 'Kathryn Day', 'Female', '2015-07-21'),
(51, 'Nicole Torres', 'Female', '2015-06-03'),
(52, 'William Garcia', 'Male', '2015-08-21'),
(53, 'Jane Rogers', 'Female', '2015-12-31'),
(54, 'John', 'Male', '2016-05-16'),
(55, 'Lora', 'Female', '2016-05-16'),
(56, 'Max', 'Male', '2016-05-16'),
(57, 'Mary', 'Female', '2016-05-16');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `group`
--
ALTER TABLE `group`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `group`
--
ALTER TABLE `group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=58;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
