<?php 
include_once('helpers/db.php');

$student = [];
$id = $_GET['id'];

connect('localhost', 'root', '', 'groupdb', function() {
	$id = $GLOBALS['id'];

	if ($_POST) {
		$name = $_POST['name'];
		$gender = $_POST['gender'];
		$birthdate = $_POST['birthdate'];
		$sql = "UPDATE students SET name='$name', gender='$gender', birthdate='$birthdate' WHERE id = $id";

		request($sql, false, function() { header('Location: index.php'); });
	}

	request("SELECT * FROM students WHERE id = $id", true, function($response) {
		$GLOBALS['student'] = $response[0];		
	});
});
?>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Edit student</title>
		<link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/main.css">
	</head>
	<body>
		<div class="container">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Edit student info</h3>
				</div>
				<div class="panel-body">
					<form action=<?php echo "$_PHP_SELF?id=".$_GET['id'] ?> method="POST" role="form">
						<legend>Edit student info</legend>
						<div class="form-group">
							<label for="name">Name</label>
							<input type="text" class="form-control" id="name" name="name" value=<?php echo $student['name'] ?>>
						</div>
						<div class="form-group">
							<label for="gender">Gender</label>
							<input type="text" class="form-control" id="gender" name="gender" size=15  value=<?php echo $student['gender'] ?>>
						</div>
						<div class="form-group">
							<label for="birthdate">Birthdate</label>
							<input type="text" class="form-control" id="birthdate" name="birthdate" size=15 value=<?php echo $student['birthdate'] ?>>
						</div>
						<div class="flex">
							<input type="submit" class="btn btn-primary" value="Save" />
							<a href=<?php echo "delete_student.php?id=$id"; ?> class="btn btn-danger">Delete</a>
							<input type="reset" class="btn btn-warning" value="Restore" />
							<a href="index.php" class="btn btn-default">Cancel</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>
	</html>