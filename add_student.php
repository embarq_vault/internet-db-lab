<?php 
include_once('helpers/db.php');

connect('localhost', 'root', '', 'groupdb', function() {
	if ($_POST) {
		$name = $_POST['name'];
		$gender = $_POST['gender'];
		$birthdate = $_POST['birthdate'];
		$sql = "INSERT INTO students (name, gender, birthdate) VALUES('$name', '$gender', '$birthdate')";

		request($sql, false, function() { header('Location: index.php'); });
	}
});
?>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Add student</title>
	<link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
	<div class="container">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h1 class="panel-title">Add new student</h1>
			</div>
			<div class="panel-body">
				<form action="<?php $_PHP_SELF ?>" method="POST" role="form">
					<div class="form-group">
						<label for="name">Name</label>
						<input type="text" class="form-control" id="name" name="name" placeholder="John" required />
					</div>
					<div class="form-group">
						<select name="gender" class="btn btn-default" required>
							<option selected>Gender</option>
							<option value="Male">Male</option>
							<option value="Female">Female</option>
						</select>
					</div>
					<div class="form-group">
						<label for="birthdate">Birthdate</label>
						<input type="date" class="form-control" id="birthdate" name="birthdate" required />
					</div>
					<div class="flex">
						<input type="submit" class="btn btn-primary" value="Save" />
						<input type="reset" class="btn btn-warning" value="Reset" />
						<a href="index.php" class="btn btn-default">Cancel</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>