<?php
  // Заполнить под себя
  $host     = 'localhost';
  $user     = 'root';
  $pass     = '';
  $db       = 'students_db';
  $students = [];

  $connection = mysql_connect($host, $user, $pass);
  if (!$connection) {
    die("Connection error: ".mysql_error());
    exit;
  }

  if (!mysql_selectdb($db)) {
    echo "Unable to select DB($db): ".mysql_error();
    exit;
  }

  $filter_query = "";
  $filter_pattern = $_POST['filter_pattern'];
  if ($filter_pattern) {
    $filter_query = "WHERE students.name LIKE '%$filter_pattern%'";
  }

  $table = 'students';
  $sql = "SELECT * FROM $table $filter_query";
  $response = mysql_query($sql);
  if (!$response) {
    echo "Could not successfully run query $sql from DB: ".mysql_error();
    exit;
  }

  while ($result = mysql_fetch_assoc($response)) {
    $students[] = $result;
  };

  mysql_free_result($response);
  mysql_close($connection);
?>

<html>
<head>
  <meta charset="UTF-8">
  <title>Data filter</title>
  <style type="text/css">
  body {
    font-family: Tahoma;
  }

  table {
    border-collapse: collapse;
    width: 60%;
  }

  td {
    border: 1px solid #ccc;
    border-spacing: 0;
    padding: 2%;
  }
  </style>
</head>
<body>
  <main>
    <form action="<?php $_PHP_SELF ?>" method="POST">
      <input type="text" name="filter_pattern" value="<?php $_POST['filter_pattern'] ?>" />
      <input type="submit" value="Refresh" />
    </form>
    <table>
      <thead>
        <tr>
          <td>Id</td>
          <td>Name</td>
          <td>Gender</td>
          <td>Birthdate</td>
        </tr>
      </thead>
      <tbody>
      <?php 
        foreach ($students as $student) {
          echo "<tr>";
          foreach ($student as $key => $value) echo "<td>$value</td>";
          echo "</tr>";
        }
      ?>
      </tbody>
    </table>
  </main>
</body>
</html>
<!-- SQL query for import db
-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 17 2016 г., 06:42
-- Версия сервера: 5.5.48
-- Версия PHP: 5.4.45

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `students_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `id` int(11) NOT NULL,
  `name` varchar(15) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `birthdate` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `students`
--

INSERT INTO `students` (`id`, `name`, `gender`, `birthdate`) VALUES
(1, 'Alan James', 'Male', '1986-12-25'),
(2, 'Adam Taylor', 'Male', '1996-01-17'),
(3, 'John King', 'Male', '1983-05-25'),
(4, 'Denise Sanders', 'Female', '1995-12-22'),
(5, 'Jessica Lane', 'Female', '1992-01-02'),
(6, 'Russell Peterso', 'Male', '1988-10-29'),
(7, 'Aaron Roberts', 'Male', '1997-02-05'),
(8, 'Thomas Parker', 'Male', '1993-05-17'),
(9, 'Joyce Frazier', 'Female', '1991-06-21'),
(10, 'Chris Ramos', 'Male', '1986-09-18'),
(11, 'Jean Sullivan', 'Female', '1987-06-25'),
(12, 'Ruby Burke', 'Female', '1989-06-09'),
(13, 'Jennifer Peters', 'Female', '1987-05-25'),
(14, 'Helen Collins', 'Female', '1983-03-18'),
(15, 'Tina Wagner', 'Female', '1985-06-03'),
(16, 'Judith Long', 'Female', '1994-11-23'),
(17, 'Nancy Mason', 'Female', '1981-03-30'),
(18, 'Amanda Lane', 'Female', '1997-06-17'),
(19, 'Nicholas Long', 'Male', '1987-01-30'),
(20, 'Lori Miller', 'Female', '1983-11-28'),
(21, 'Heather Reyes', 'Female', '1982-12-07'),
(22, 'Joyce Bryant', 'Female', '1997-05-20'),
(23, 'Kenneth Wright', 'Male', '1983-03-06'),
(24, 'Debra Daniels', 'Female', '1981-04-07'),
(25, 'Samuel Kim', 'Male', '1984-01-16'),
(26, 'Donna Willis', 'Female', '1995-12-09'),
(27, 'Cheryl Jackson', 'Female', '1995-03-17'),
(28, 'Lisa Cox', 'Female', '1987-08-18'),
(29, 'Joseph Gordon', 'Male', '1997-06-07'),
(30, 'Kathy Fisher', 'Female', '1990-10-03'),
(31, 'Marie Brown', 'Female', '1993-09-11'),
(32, 'Keith Williams', 'Male', '1994-04-01'),
(33, 'Harry Stone', 'Male', '1988-05-06'),
(34, 'Tina Ford', 'Female', '1990-05-10'),
(35, 'Joyce Hughes', 'Female', '1982-05-11'),
(36, 'Jonathan Turner', 'Male', '1982-07-13'),
(37, 'Steven Schmidt', 'Male', '1992-07-19'),
(38, 'Pamela Jones', 'Female', '1996-02-29'),
(39, 'Patrick Burns', 'Male', '1985-01-15'),
(40, 'Peter Russell', 'Male', '1984-12-17'),
(41, 'Wayne Hicks', 'Male', '1985-08-03'),
(42, 'Kenneth Hawkins', 'Male', '1980-03-03'),
(43, 'Amy Moreno', 'Female', '1990-10-02'),
(44, 'Sara Garcia', 'Female', '1995-10-22'),
(45, 'Chris Wilson', 'Male', '1995-05-06'),
(46, 'Clarence Warren', 'Male', '1989-10-29'),
(47, 'Anne Snyder', 'Female', '1995-07-29'),
(48, 'Judy Nguyen', 'Female', '1993-12-21'),
(49, 'Wayne Fisher', 'Male', '1983-09-05'),
(50, 'Ralph Stevens', 'Male', '1990-11-02'),
(51, 'Betty Stevens', 'Female', '1987-12-01'),
(52, 'Bonnie Bailey', 'Female', '1988-12-22'),
(53, 'Kenneth Mills', 'Male', '1986-03-27'),
(54, 'Mildred Larson', 'Female', '1981-09-07'),
(55, 'Charles Wells', 'Male', '1998-06-28'),
(56, 'Kathy Gonzalez', 'Female', '1985-08-04'),
(57, 'Charles King', 'Male', '1988-05-10'),
(58, 'Tammy Warren', 'Female', '1986-04-29'),
(59, 'Earl Evans', 'Male', '1983-12-06'),
(60, 'Amanda Richards', 'Female', '1980-09-15'),
(61, 'Edward Miller', 'Male', '1987-03-22'),
(62, 'Harold Frazier', 'Male', '1983-07-28'),
(63, 'Heather Chapman', 'Female', '1982-04-27'),
(64, 'Joan Lawrence', 'Female', '1983-11-09'),
(65, 'Paula Greene', 'Female', '1990-06-13'),
(66, 'Kathleen Jackso', 'Female', '1984-02-02'),
(67, 'Patricia Mcdona', 'Female', '1990-07-02'),
(68, 'Dorothy Cooper', 'Female', '1981-07-03'),
(69, 'Victor Fernande', 'Male', '1984-04-13'),
(70, 'Andrew Kennedy', 'Male', '1985-02-14'),
(71, 'Larry Walker', 'Male', '1984-01-28'),
(72, 'Christopher Fos', 'Male', '1988-05-01'),
(73, 'Mildred Allen', 'Female', '1986-11-30'),
(74, 'Doris Day', 'Female', '1987-10-08'),
(75, 'Barbara Harris', 'Female', '1981-05-24'),
(76, 'Howard Vasquez', 'Male', '1989-08-09'),
(77, 'David Arnold', 'Male', '1991-05-17'),
(78, 'Scott Brooks', 'Male', '1989-10-01'),
(79, 'Chris Holmes', 'Male', '1989-04-15'),
(80, 'Sara Palmer', 'Female', '1998-05-31'),
(81, 'Gerald Harper', 'Male', '1987-11-27'),
(82, 'Joshua Ferguson', 'Male', '1995-05-26'),
(83, 'Jerry Alexander', 'Male', '1986-07-27'),
(84, 'Jerry Brooks', 'Male', '1982-05-31'),
(85, 'Antonio Weaver', 'Male', '1987-01-22'),
(86, 'Fred Arnold', 'Male', '1987-02-03'),
(87, 'Robert Ryan', 'Male', '1986-05-17'),
(88, 'Jack Green', 'Male', '1992-11-30'),
(89, 'Kathleen Stewar', 'Female', '1997-02-12'),
(90, 'Jeremy Edwards', 'Male', '1984-02-07'),
(91, 'Marie Evans', 'Female', '1991-07-08'),
(92, 'Frances Coleman', 'Female', '1982-03-02'),
(93, 'Margaret Woods', 'Female', '1982-01-10'),
(94, 'Judith Turner', 'Female', '1983-08-04'),
(95, 'Anne Moreno', 'Female', '1987-09-09'),
(96, 'Jose Griffin', 'Male', '1983-08-07'),
(97, 'Jose Mccoy', 'Male', '1998-07-29'),
(98, 'Victor Wells', 'Male', '1984-10-19'),
(99, 'Gary Washington', 'Male', '1996-01-06'),
(100, 'Chris Smith', 'Male', '1997-07-24');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=101;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

End of the SQL query -->