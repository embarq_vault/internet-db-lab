<?php 
	include_once('helpers/db.php');

	$group = [];
	$students = [];
	$filter_query = "";

	connect('localhost', 'root', '', 'groupdb', function() {
		request('SELECT * FROM `group`', true, function($response) {
			$GLOBALS['group'] = [
				'number' => $response[0]['number'],
				'faculty' => $response[0]['faculty'],
				'lead' => $response[0]['lead']
			];
		});	// request

		$filter_pattern = $_POST['filter_pattern'];
		if ($filter_pattern) {
			$filter_query = "WHERE students.name LIKE '%$filter_pattern%'";
		}

		request("SELECT * FROM students $filter_query", true, function($response) {
			foreach ($response as $student) {
				$GLOBALS['students'][] = $student;
			}
		});	// request
	})
?>
<html>
<head>
	<title>InterBD Labs</title>
	<link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
	<div class="container">
		<div class="panel panel-primary">
			<div class="flex panel-heading">
				<h1 class="panel-title">Group info</h1>
				<a href="edit_group.php" class="btn btn-primary">Edit</a>
			</div>
			<div class="panel-body">
				<h1 class="text-primary">Group - <?php echo $group['number'] ?></h1>
				<h4>Faculty - <?php echo $group['faculty'] ?></h4>
				<h4>Lead - <?php echo $group['lead'] ?></h4>
				<table class="table table-info table-hover">
					<caption>
						<h1>Students</h1>
						<!-- FILTER -->
						<form action="<?php $_PHP_SELF ?>" method="POST">
							<div class="input-group">
								<input type="text" class="form-control" name="filter_pattern" placeholder="Enter smth" value="<?php $_POST['filter_pattern'] ?>" />
								<span class="input-group-addon"><button type="submit">Refresh</button></span>
							</div>
						</form>
						<!-- FILTER -->
					</caption>
					<thead class="bg-primary">
						<tr>
							<td>Name</td>
							<td>Gender</td>
							<td>Birthdate</td>
						</tr>
					</thead>
					<tbody>
						<?php 
						try {
							foreach ($students as $student) {
								$id = $student['id'];
								$class = 'male';
								if ($student['gender'] == 'Female') $class = 'female';
								echo "<tr class=$class data-href='edit_student.php?id=$id' title='Edit'>";
								foreach ($student as $key => $value) if ($key != 'id') echo "<td>$value</td>";
								echo "</tr>";
							}
						} catch (Exception $e) {
							echo $e;
						}
						?>
						<tr data-href="add_student.php">
							<td colspan=3 style="text-align: center">Add student</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript">jQuery(document).ready(function(t){t("tbody > tr").click(function(){window.document.location=t(this).data("href")})});</script>
</html>